# Production deployment

MDAS final project deployment repository.

## Kubernetes cluster

Kubernetes is expected to be deployed with all components.

### Description

* Storage
  * [Rook opertator](https://rook.io/)
  * Simplified cluster (test purposes)
  * Total storage 100GiB
  * Version deployed 1.4
  * Cluster type: Ceph (RDB)

  ```console
  git clone https://github.com/rook/rook.git
  cd rook && git checkout release-1.4
  cd cluster/examples/kubernetes/ceph/
  kubectl apply -f common.yaml
  kubectl apply -f operator.yaml
  kubectl apply -f cluster-test.yaml
  kubectl apply -f csi/rbd/storageclass-test.yaml
  kubectl patch storageclass rook-ceph-block -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
  ```

* Monitoring stack
  * [kube-prometheus](https://github.com/prometheus-operator/kube-prometheus/)
  * Components
    * Prometheus
    * Alertmanager
    * Grafana
  * Cluster and application monitoring
  * [Customized](assets/tfm-kube-prometheus) to monitor all namespaces used for this project deployment and exposing Grafana using an Ingress resource.
  * Once [customized](https://github.com/prometheus-operator/kube-prometheus/#customizing-kube-prometheus), apply as follows:

  ```console
  kubectl apply -f manifests/setup
  kubectl wait --for condition=Ready pod -l app.kubernetes.io/name=prometheus-operator -n monitoring
  kubectl apply -f manifests
  ```

* Sensor platform
  * [Same automation used in development](https://gitlab.com/mdas_tfm/minikube-env)
  * Components
    * Kafka
    * ElastiSearch
    * Grafana

## Application deployment

Applications are deployed to Kubernetes from Gitlab using the [deployment script](deploy.sh).

The script can be used manually or from the CI/CD pipeline passing the argumet *ci*, which then, based on the last commit log, will deploy the appropiate application from the [assets directory](assets/).

The script will monitor for the deployments to roll out successfully, if they don't the script will attempt a rollback by checking out the N-1 tag and using the manifests found on it for the failing deployment. It will again monitor the rollback process and exit unsuccessufully so a human gets notified that the CD pipeline failed.
