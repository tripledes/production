#!/bin/bash

export SENSOR_NAMESPACE="tfm"
export SENSOR_AREA_NAME="Barcelona"
export SENSOR_GW_ADDRESS="gateway.tfm.svc.cluster.local"
export LOCATION="41.23,2.11"
export SENSOR_INTERVAL="15s"
export SENSOR_METRICS_PORT="2112"
export SENSOR_NAME="sensor-barcelona"
